USE TransportCompany
GO

DROP TABLE CurrentVersion
DROP TABLE Proced
DROP TABLE OppositeProced

CREATE TABLE CurrentVersion(nb INT)
INSERT INTO CurrentVersion VALUES (0)
SELECT * FROM CurrentVersion

UPDATE CurrentVersion
SET nb=0

CREATE TABLE Proced(nb INT,prc VARCHAR(100))
INSERT INTO Proced VALUES(1,'uspModifyColumnType')
INSERT INTO Proced VALUES(2,'uspAddColumn')
INSERT INTO Proced VALUES(3,'uspAddDefaultConstraint')
INSERT INTO Proced VALUES(4,'uspAddPrimaryKeyConstraint')
INSERT INTO Proced VALUES(5,'uspAddCandidateKeyConstraint')
INSERT INTO Proced VALUES(6,'uspAddForeignKeyConstraint')
INSERT INTO Proced VALUES(7,'uspCreateTable')
SELECT * FROM Proced


CREATE TABLE OppositeProced(nb INT,prc VARCHAR(100))
INSERT INTO OppositeProced VALUES(1,'uspUndoModifyColumnType')
INSERT INTO OppositeProced VALUES(2,'uspRemoveColumn')
INSERT INTO OppositeProced VALUES(3,'uspRemoveDefaultConstraint')
INSERT INTO OppositeProced VALUES(4,'uspRemovePrimaryKeyConstraint')
INSERT INTO OppositeProced VALUES(5,'uspRemoveCandidateKeyConstraint')
INSERT INTO OppositeProced VALUES(6,'uspRemoveForeignKeyConstraint')
INSERT INTO OppositeProced VALUES(7,'uspRemoveTable')
SELECT * FROM OppositeProced

/*
DECLARE @procedureName NVARCHAR(100) 
SELECT @procedureName=Proced.prc
FROM Proced
WHERE Proced.nb=8
EXECUTE sp_executesql @procedureName
GO
*/

CREATE PROCEDURE uspMain(@version INT)
AS
	DECLARE @procedureName NVARCHAR(100)
	DECLARE @crtVersion INT =( SELECT CurrentVersion.nb FROM CurrentVersion )
	WHILE @crtVersion != @version
	BEGIN
		IF @crtVersion < @version
		BEGIN
			SET @crtVersion = @crtVersion+1
			UPDATE CurrentVersion
			SET nb=@crtVersion

			SELECT @procedureName=Proced.prc
			FROM Proced
			WHERE Proced.nb=@crtVersion
			EXECUTE sp_executesql @procedureName

			IF @crtVersion = @version
			BEGIN
				BREAK
			END
		END

		IF @crtVersion > @version
		BEGIN
			SELECT @procedureName = OppositeProced.prc
			FROM OppositeProced
			WHERE OppositeProced.nb=@crtVersion
			EXECUTE sp_executesql @procedureName

			SET @crtVersion=@crtVersion-1
			UPDATE CurrentVersion
			SET nb=@crtVersion

			IF @crtVersion = @version
			BEGIN
				BREAK
			END
		END
	END
GO
EXEC uspMain 0
DROP PROCEDURE uspMain

select * from sys.objects where type ='UQ'
