USE TransportCompany 
GO

INSERT INTO Tables(Name) VALUES('truck')
INSERT INTO Tables(Name) VALUES('driver')
INSERT INTO Tables(Name) VALUES('tickets')

INSERT INTO Views(Name) VALUES('View_1')
INSERT INTO Views(Name) VALUES('View_2')
INSERT INTO Views(Name) VALUES('View_3')

INSERT INTO Tests(Name) VALUES('insert')
INSERT INTO Tests(Name) VALUES('delete')
INSERT INTO Tests(Name) VALUES('select')

INSERT INTO TestViews(TestID,ViewID) VALUES(3,1)
INSERT INTO TestViews(TestID,ViewID) VALUES(3,2)
INSERT INTO TestViews(TestID,ViewID) VALUES(3,3)

INSERT INTO TestTables(TestID,TableID,NoOfRows,Position) VALUES(1,1,100,1)
INSERT INTO TestTables(TestID,TableID,NoOfRows,Position) VALUES(1,2,100,1)
INSERT INTO TestTables(TestID,TableID,NoOfRows,Position) VALUES(1,3,100,1)
SELECT * FROM TestTables
SELECT * FROM Tables


INSERT INTO TestTables(TestID,TableID,NoOfRows,Position) VALUES(2,3,100,1)
INSERT INTO TestTables(TestID,TableID,NoOfRows,Position) VALUES(2,2,100,1)
INSERT INTO TestTables(TestID,TableID,NoOfRows,Position) VALUES(2,1,100,1)




ALTER PROCEDURE uspInsertIntoTruck
AS
	DECLARE @counter INT 
	DECLARE @rows1 INT
	DECLARE @nbOfRows INT
	SELECT @counter = MAX(tid) FROM truck


	SELECT @nbOFRows=NoOfRows FROM dbo.TestTables TT,dbo.Tables T
	WHERE TT.TableID=T.TableID and T.Name like 'truck'

	SELECT @rows1=COUNT(*) FROM truck
	IF @rows1 = 0 SET @counter=1

	WHILE @nbOfRows > 0
	BEGIN
		SET @counter=@counter +1 
		INSERT INTO truck VALUES(@counter,RAND()*(40-30)+30,'Truck type')
		SET @nbOfRows=@nbOfRows-1
	END
GO

ALTER PROCEDURE uspDeleteFromTruck
AS
	DECLARE @rowCount INT
	DECLARE @nbOfRows INT
	SELECT @rowCount=COUNT(*) FROM truck
	DECLARE @maxID INT
	
	SELECT @nbOFRows=NoOfRows FROM dbo.TestTables TT,dbo.Tables T
	WHERE TT.TableID=T.TableID and T.Name like 'truck'

	WHILE @nbOfRows > 0
	BEGIN
		IF @rowCount!=0 RAISERROR('The table truck is empty!',10,1)
		SELECT @maxID=MAX(tid) FROM truck

		DELETE FROM truck WHERE tid=@maxID
		SET @rowCount=@rowCount-1
		SET @nbOfRows=@nbOfRows-1
	END
GO

ALTER PROCEDURE uspInsertIntoDriver 
AS
	DECLARE @counter INT 
	DECLARE @rows1 INT
	DECLARE @tid INT
	DECLARE @nbOfRows INT
	SELECT @counter = MAX(tid) FROM driver

	SELECT @nbOFRows=NoOfRows FROM dbo.TestTables TT,dbo.Tables T
	WHERE TT.TableID=T.TableID and T.Name like 'driver'

	SELECT @rows1=COUNT(*) FROM driver
	IF @rows1 = 0 SET @counter=1

	WHILE @nbOfRows > 0
	BEGIN
		SET @counter=@counter +1 
		SELECT TOP 1 @tid=tid FROM truck
		INSERT INTO driver VALUES(@counter,'Driver name','Date of birth','License category',@tid)
		SET @nbOfRows=@nbOfRows-1
	END
GO

ALTER PROCEDURE uspDeleteFromDriver(@nbOfRows INT)
AS
	DECLARE @rowCount INT
	DECLARE @nbOfRows INT
	SELECT @rowCount=COUNT(*) FROM driver
	DECLARE @maxID INT
	
	SELECT @nbOFRows=NoOfRows FROM dbo.TestTables TT,dbo.Tables T
	WHERE TT.TableID=T.TableID and T.Name like 'driver'

	WHILE @nbOfRows > 0
	BEGIN
		IF @rowCount!=0 RAISERROR('The table driver is empty!',10,1)
		SELECT @maxID=MAX(did) FROM driver 

		DELETE FROM driver WHERE did=@maxID
		SET @rowCount=@rowCount-1
		SET @nbOfRows=@nbOfRows-1
	END
GO

ALTER PROCEDURE uspInsertIntoTicket 
AS
	DECLARE @counter INT 
	DECLARE @rows1 INT
	DECLARE @did INT
	DECLARE @nbOfRows INT
	SELECT @counter = MAX(tkid) FROM ticket

	SELECT @nbOFRows=NoOfRows FROM dbo.TestTables TT,dbo.Tables T
	WHERE TT.TableID=T.TableID and T.Name like 'ticket'

	SELECT @rows1=COUNT(*) FROM ticket
	IF @rows1 = 0 SET @counter=1

	WHILE @nbOfRows > 0
	BEGIN
		SET @counter=@counter +1 
		SELECT TOP 1 @did=did FROM driver
		INSERT INTO ticket VALUES(@counter,RAND()*(1000-950)+950,@did)
		SET @nbOfRows=@nbOfRows-1
	END
GO

ALTER PROCEDURE uspDeleteFromTicket
AS
	DECLARE @rowCount INT
	DECLARE @nbOfRows INT
	SELECT @rowCount=COUNT(*) FROM ticket
	DECLARE @maxID INT
	
	SELECT @nbOFRows=NoOfRows FROM dbo.TestTables TT,dbo.Tables T
	WHERE TT.TableID=T.TableID and T.Name like 'ticket'

	WHILE @nbOfRows > 0
	BEGIN
		IF @rowCount!=0 RAISERROR('The table ticket is empty!',10,1)
		SELECT @maxID=MAX(tkid) FROM ticket

		DELETE FROM ticket WHERE tkid=@maxID
		SET @rowCount=@rowCount-1
		SET @nbOfRows=@nbOfRows-1
	END
GO

ALTER PROCEDURE uspInsertAll
AS
	DECLARE @beforeInsertTruck DATETIME
	DECLARE @afterInsertTruck DATETIME
	DECLARE @beforeInsertDriver DATETIME
	DECLARE @afterInsertDriver DATETIME
	DECLARE @beforeInsertTicket DATETIME
	DECLARE @afterInsertTicket DATETIME

	SET @beforeInsertTruck= GETDATE()
	EXEC uspInsertIntoTruck
	SET @afterInsertTruck= GETDATE()

	SET @beforeInsertDriver= GETDATE()
	EXEC uspInsertIntoDriver
	SET @afterInsertDriver= GETDATE()

	SET @beforeInsertTicket= GETDATE()
	EXEC uspInsertIntoTicket
	SET @afterInsertTicket= GETDATE()

GO

ALTER PROCEDURE uspDeleteAll
AS
	DECLARE @beforeDeleteTruck DATETIME
	DECLARE @afterDeleteTruck DATETIME
	DECLARE @beforeDeleteDriver DATETIME
	DECLARE @afterDeleteDriver DATETIME
	DECLARE @beforeDeleteTicket DATETIME
	DECLARE @afterDeleteTicket DATETIME

	SET @beforeDeleteTruck= GETDATE()
	EXEC uspDeleteFromTicket
	SET @afterDeleteTruck= GETDATE()
	
	SET @beforeDeleteDriver= GETDATE()
	EXEC uspDeleteFromDriver
	SET @afterDeleteDriver= GETDATE()
	
	SET @beforeDeleteTicket= GETDATE()
	EXEC uspDeleteFromTruck
	SET @afterDeleteTicket= GETDATE()
GO


EXEC uspDeleteAll
SELECT * FROM truck
SELECT * FROM driver
SELECT * FROM ticket 















