--/*
drop table ticket
drop table driver
drop table cargo
drop table repair
drop table transportRoute
drop table parkedTruck
drop table truck
DROP TABLE highway
drop table rroute 
drop table sender
drop table receiver
drop table parkingSpot
drop table parking
drop table mechanic
DROP PROCEDURE uspModifyColumnType
DROP PROCEDURE uspUndoModifyColumnType
DROP PROCEDURE uspAddColumn
DROP PROCEDURE uspRemoveColumn
DROP PROCEDURE uspAddDefaultConstraint
DROP PROCEDURE uspRemoveDefaultConstraint
DROP PROCEDURE uspAddPrimaryKeyConstraint
DROP PROCEDURE uspRemovePrimaryKeyConstraint
DROP PROCEDURE uspAddCandidateKeyConstraint
DROP PROCEDURE uspRemoveCandidateKeyConstraint
DROP PROCEDURE uspAddForeignKeyConstraint
DROP PROCEDURE uspRemoveForeignKeyConstraint
DROP PROCEDURE uspCreateTable
DROP PROCEDURE uspRemoveTable
--*/

CREATE TABLE parking
(
	pid INT PRIMARY KEY,parkingAddress VARCHAR(50)
)

CREATE TABLE parkingSpot /*drop*/
(
	pNumber INT,
	pid INT FOREIGN KEY REFERENCES parking(pid),
	PRIMARY KEY(pNumber,pid)
)

CREATE TABLE truck
(
	tid INT PRIMARY KEY,capacity INT,truckType VARCHAR(50),
)

CREATE TABLE parkedTruck
(
	pNumber INT,pid INT,
	tid INT FOREIGN KEY REFERENCES truck(tid),
	dateAndTime DATETIME,
	FOREIGN KEY (pNumber,pid) REFERENCES parkingSpot,
	PRIMARY KEY(pNumber,pid,tid,dateAndTime)
)

CREATE TABLE driver
(
	did INT PRIMARY KEY,driverName VARCHAR(50) NOT NULL,
	driverDOB VARCHAR(50),
	licenseCategory VARCHAR(50),
	tid INT FOREIGN KEY REFERENCES truck(tid) /*unique*/
)

CREATE TABLE mechanic
(
	mid INT PRIMARY KEY,mechanicName VARCHAR(50) NOT NULL
)

CREATE TABLE sender
(
	seid INT PRIMARY KEY, senderName VARCHAR(50), senderAddress VARCHAR(50)
)

CREATE TABLE receiver
(
	reid INT PRIMARY KEY,receiverName VARCHAR(50),receiverAddress VARCHAR(50),
)

CREATE TABLE cargo
(
	caid INT PRIMARY KEY,cargoWeight INT,origin VARCHAR(50),destination VARCHAR(50),
	category VARCHAR(50),
	tid INT FOREIGN KEY REFERENCES truck(tid),
	seid INT FOREIGN KEY REFERENCES sender(seid),
	reid INT FOREIGN KEY REFERENCES receiver(reid)
)

CREATE TABLE repair
(
	tid INT FOREIGN KEY REFERENCES truck(tid),
	mid INT FOREIGN KEY REFERENCES mechanic(mid),
	repairDateTime DATETIME,
	PRIMARY KEY(tid,mid,repairDateTime)
)

CREATE TABLE highway
(
	hid INT UNIQUE NOT NULL,
	highwayLength INT NOT NULL,
	highwayName VARCHAR (100)
)

CREATE TABLE rroute
(
	rid INT PRIMARY KEY, 
	startingPoint VARCHAR(50), 
	endingPoint VARCHAR(50),
	routeLength INT,
	hid INT FOREIGN KEY REFERENCES highway(hid)
)

CREATE TABLE transportRoute
(
	tid INT FOREIGN KEY REFERENCES truck(tid),
	rid INT FOREIGN KEY REFERENCES rroute(rid),
	transportDateTime DATETIME,
	PRIMARY KEY(tid,rid,transportDateTime)
)

CREATE TABLE ticket
(
	tkid INT, 
	fine INT,
	did INT FOREIGN KEY REFERENCES driver(did),
	PRIMARY KEY(tkid,fine)
)

/*INSERT INTO parking VALUES ('Bucuresti, str. Parcarii, nr. 10',1) Violates integrity constraints*/
INSERT INTO parking VALUES (1,'Bucuresti, str. Parcarii, nr. 10')
INSERT INTO parking VALUES (2,'Cluj-Napoca, str. Stradescu, nr. 50')
INSERT INTO parking VALUES (3,'Timisoara, str. Revolutiei, nr. 16')
INSERT INTO parking VALUES (4,'Iasi, str. Copou, nr. 100')

UPDATE parking 
SET parkingAddress='Iasi, str. Copou, nr. 101'
WHERE pid=4


/*INSERT INTO truck VALUES('1','Volvo',25)Violates integrity constraints*/
INSERT INTO truck VALUES(1,25,'Volvo')
INSERT INTO truck VALUES(2,20,'Mercedes')
INSERT INTO truck VALUES(3,30,'Man')
INSERT INTO truck VALUES(4,25,'Scania')
INSERT INTO truck VALUES(5,20,'Reanult')
INSERT INTO truck VALUES(6,30,'Ford')
SELECT * FROM truck  

UPDATE truck 
SET capacity=20
WHERE tid=4

DELETE FROM truck WHERE truckType='Man'

/*
INSERT INTO driver VALUES(1,44,'D',Nelu) Violates integrity constraints*/
INSERT INTO driver VALUES(1,'Nelu','15.05.1974','F',1)
INSERT INTO driver VALUES(2,'Robi','20,.01.1980','A',2)
INSERT INTO driver VALUES(3,'Sandu','10.10.1990','C',1)
INSERT INTO driver VALUES(4,'Liviu','01.01.1970','D',4)
INSERT INTO driver VALUES(5,'Liviu','15.11.1988','A',6)
SELECT * FROM driver 

UPDATE driver 
SET driverDOB='10.01.1970'
WHERE did=4

DELETE FROM driver WHERE licenseCategory='D'

/*
INSERT INTO mechanic VALUES('Gicu',1)Violates integrity constraints*/
INSERT INTO mechanic VALUES(1,'Gicu') 
INSERT INTO mechanic VALUES(2,'Viorel')
INSERT INTO mechanic VALUES(3,'Mircea')
INSERT INTO mechanic VALUES(4,'Lica')
SELECT * FROM mechanic

INSERT INTO sender VALUES(1,'Cristian','Cluj-Napoca, str. Aurel Vlaicu, nr.10')
INSERT INTO sender VALUES(2,'George','Bucuresti, str. Aviatorilor, nr.15')
INSERT INTO sender VALUES(3,'Vasile','Sibiu, str. Sibienilor, nr. 18')
SELECT * FROM sender 

INSERT INTO receiver VALUES(1,'Alexandru','Timisoara, str. Ciocarliei, nr, 150')
INSERT INTO receiver VALUES(2,'Andrei','Timisoara, str. Pasarilor, nr, 74')
INSERT INTO receiver VALUES(3,'George','Oradea, str. Scortarilor, nr. 80')
INSERT INTO receiver VALUES(4,'Alin','Sibiu, str. Ursului, nr. 21')
INSERT INTO receiver VALUES(5,'Alina','Brasov, str. Unirii, nr. 192')
SELECT * FROM receiver 

INSERT INTO parkingSpot VALUES(12,2)
INSERT INTO parkingSpot VALUES(1,1)
INSERT INTO parkingSpot VALUES(30,1)

INSERT INTO parkedTruck VALUES(12,2,4,'20180830 20:10:30:001')
INSERT INTO parkedTruck VALUES(1,1,4,'20180831 15:36:10:000')
INSERT INTO parkedTruck VALUES(30,1,2,'20180831 21:16:45:000')
INSERT INTO parkedTruck VALUES(1,1,4,'2018-07-20')
SELECT * FROM parkedTruck

INSERT INTO cargo VALUES(1,2,'Cluj-Napoca','Bucuresti','D',1,1,1)
INSERT INTO cargo VALUES(2,3,'Baia-Mare','Oradea','A',1,2,1)
INSERT INTO cargo VALUES(3,20,'Cluj-Napoca','Brasov','C',6,1,5)
INSERT INTO cargo VALUES(4,20,'Sibiu','Bucuresti','C',6,3,5)
INSERT INTO cargo VALUES(5,10,'Cluj-Napoca','Brasov','C',6,1,5)
INSERT INTO cargo VALUES(6,2,'Baia-Mare','Bucuresti','C',6,1,5)
SELECT * FROM cargo

UPDATE cargo
SET tid=1
WHERE caid=3

INSERT INTO rroute VALUES(1,'Cluj-Napoca','Bucuresti',100)
INSERT INTO rroute VALUES(2,'Baia-Mare','Oradea',1000)
INSERT INTO rroute VALUES(3,'Timisoara','Satu-Mare',55)
INSERT INTO rroute VALUES(4,'Cluj-Napoca','Sibiu',230)
INSERT INTO rroute VALUES(5,'Bucuresti','Satu-Mare',330)
INSERT INTO rroute VALUES(6,'Cluj-Napoca','Bucuresti',464)
SELECT * FROM rroute

UPDATE rroute
SET endingPoint='Satu-Mare'
WHERE rid=5

INSERT INTO transportRoute VALUES(1,1,'20180110 20:15:20:000')
INSERT INTO transportRoute VALUES(4,1,'20180110 20:15:20:000')
INSERT INTO transportRoute VALUES(1,3,'20180801 11:10:21:000')
SELECT * FROM transportRoute
UPDATE transportRoute
SET rid=2
WHERE tid=4
/*
	UNION ALL
	Find the ids of trucks with type Volvo or were repaired by a mechanic with id 2
	Selectati ID-urile camioanelor care sunt de tipul Volvo sau care au fost reparate de un mecanic cu id 2 
*/
SELECT T.tid
FROM truck T
WHERE T.truckType='Volvo'
UNION ALL
SELECT R.tid
FROM repair R
WHERE R.mid=2

/*
	OR
	Find the type of the trucks which were driven by Nelu or Robi
	Gasiti tipurile de camion care sunt conduse de Nelu sau Robi
*/ 
SELECT T.truckType
FROM truck T,driver D
WHERE T.tid=D.tid AND (D.driverName='Nelu' OR D.driverName='Robi')

/*
	INTERSECT
	Find the ids of the trucks which were parked on 30 August 2018 and 31 August 2018
	Gasiti id-urile camioanelor care au fost parcate pe 30 August 2018 si pe 31 August 2018
*/
SELECT P.tid
FROM parkedTruck P
WHERE P.dateAndTime='2018-08-30'
INTERSECT
SELECT P2.tid
FROM parkedTruck P2
WHERE P2.dateAndTime='2018-08-31'
/*
	IN
	Find the names of senders who have sent cargos weighing 2t
	Gasiti numele expeditorilor care au trimis colete ce cantaresc 2 tone
*/
SELECT S.senderName
FROM sender S
WHERE S.seid IN
	(
		SELECT C.seid
		FROM cargo C
		WHERE C.cargoWeight=2
	)

/*
	EXCEPT
	Find the ids of the trucks which had been a transport route of 100km, but had not been on a route of 1000km
	Gasiti id-urile camioanelor care au fost pe o ruta de 100 km, dar care nu au fost pe o ruta de 1000km.
*/
SELECT T.tid
FROM transportRoute T,rroute R
WHERE T.rid=R.rid AND R.routeLength=100
EXCEPT
SELECT T2.tid
FROM transportRoute T2,rroute R2
WHERE T2.rid=R2.rid AND R2.routeLength=1000

/*
	NOT IN
	Find the names of the drivers who did not drove a truck repaired by Viorel
	Gasiti numele soferilor care nu au condus un camion reparat de Viorel
*/
SELECT  D.driverName
FROM driver D
WHERE D.tid NOT IN
	(
		SELECT R.tid
		FROM repair R,mechanic M
		WHERE R.mid=M.mid AND M.mechanicName='Viorel'
	)

/*
	INNER JOIN
	Find the names and the dates of birth for the drivers who have a truck assigned.
	Gasiti numele si data nasterii soferilor care au camioane alocate.
*/
SELECT D.driverName,D.driverDOB
FROM driver D INNER JOIN truck T ON D.tid=T.tid

/*
	LEFT JOIN
	Find all trucks' details and drivers assignated
	Gasiti detaliile fiecarui camion si id-ul soferilor care conduc acel camion
*/
SELECT *
FROM truck T LEFT JOIN driver D ON T.tid=D.tid

/*
	RIGHT JOIN
	Find all the routes and the corresponding transport route
	Gasiti toate rutele si id-urile transporturilor care se realizeaza pe acea ruta
*/
SELECT *
FROM transportRoute TR RIGHT JOIN rroute R ON TR.rid=R.rid

/*
	FULL JOIN
	Find all the truks and their transport route and route		
	Gasiti toate camioanele, detaliile de transport si ruta pe care au mers
*/
SELECT *
FROM truck T FULL JOIN transportRoute TR ON T.tid=TR.tid FULL JOIN rroute R ON TR.rid=R.rid

/*
	e. IN+SUBQUERY
	Find the license category for the drivers who drive a Volvo
	Gasiti numele si licenta pe care o au soferii care conduc camioane Volvo.
*/ 
SELECT D.driverName,D.licenseCategory
FROM driver D
WHERE D.tid IN 
	(
		SELECT T.tid
		FROM truck T
		WHERE T.truckType='Volvo'
	)

/*
	e. IN+SUBQUERY +IN+SUBQUERY
	Find the name of the drivers who transported a cargo from a sender with id 1 and received by a receiver with
	id 5.
	Gasiti numele soferilor care au transportat colete de la un expeditor cu id 1 si care au fost primite de 
		un destinatar cu id 5
*/
SELECT D.driverName
FROM driver D
WHERE D.tid IN
	(
		SELECT T.tid
		FROM truck T
		WHERE T.tid IN
			(
				SELECT C.tid
				FROM cargo C
				WHERE C.seid=1 AND C.reid=5
			)
	)

/*
	f. EXIST + SUBQUERY
	Find the name of the senders who sent cargos to Bucuresti	
	Gasiti numele expeditorilor care au trimis colete catre Bucuresti
*/
SELECT S.senderName
FROM Sender S
WHERE EXISTS
	(
		SELECT *
		FROM cargo C
		WHERE C.destination='Bucuresti' AND C.seid=S.seid
	)


/*
	f. EXIST + SUBQUERY + ARITHMETIC EXPRESSION
	Find the id,capacity and the type of trucks which delivered cargos from Cluj-Napoca
	Gasiti id-ul,capacitatea si tipul camioanelor care au livrat colete din Cluj-Napoca. Scadeti din capacitatea
		camionului 3 t.
*/
SELECT T.tid,T.capacity-3,T.truckType
FROM truck T
WHERE EXISTS 
	(
		SELECT *
		FROM cargo C
		WHERE C.origin='Cluj-Napoca' AND T.tid=C.tid 
	)

/*
	g. SUBQ. IN FROM
	Find the dates in which a truck was parked on a parking spot with parking number=12
	Gasiti datele in care cel putin un camion a fost parcat pe un loc de parcare cu numarul 12.
*/
SELECT parked.dateAndTime
FROM
	(
		SELECT parkedTruck.dateAndTime as dateAndTime
		FROM parkedTruck
		WHERE parkedTruck.pNumber=12
	)AS parked

/*
	g. SUBQ. IN FROM
	Find the ids of the drivers with name 'Liviu'
	Gasiti id-urile soferilor cu numele Liviu
*/
SELECT driverName.dName
FROM
	(
		SELECT driver.driverName AS dName
		FROM driver 
		WHERE driver.driverName='Liviu'
	)AS driverName


/*
	h.GROUP BY
	Group the cargos by weight. Count the number of cargos for each weight.
	Determinati numarul de colete cu aceeasi greutate. Grupati-le dupa greutate si dupa numarul de colete, care 
		trebuie sa fie mai mare decat 1.
*/
SELECT COUNT(cargoWeight),C.cargoWeight
FROM cargo C
GROUP BY C.cargoWeight
HAVING COUNT(C.caid)>1

/*
	h.GROUP BY + ARITHMETIC EXPRESSION
	Group the cargos by destination and compute the total weight sent there.
	Grupati coletele dupa destinatie si calculati greutatea totala care a fost trimisa acolo, apoi adaugati 5 t 
		la greutatea totala
*/
SELECT C.destination,SUM(cargoWeight)+5
FROM cargo C
GROUP BY C.destination

/*
	h.GROUP BY + ARITHMETIC EXPRESSION 
	Group the routes by starting point and compute the average length.
	Grupati rutele care pornesc din acelasi punct si determinati-le pe cele care au media de km mai mica sau egala
			cu distanta minima a unei rute care are ca si punct de sosire Bucuresti. Multiplicati cu 2 media.
*/
SELECT R.startingPoint,AVG(R.routeLength)*2
FROM rroute R
GROUP BY R.startingPoint
HAVING AVG(R.routeLength)<=(
							SELECT MIN(R2.routeLength)
							FROM rroute R2 
							WHERE R2.endingPoint='Bucuresti'
						  )

/*
	h.GROUP BY
	Group the routes by ending point and compute the minimum and the maximum length of the routes
	Grupati rutele dupa punctul de sosire, selectandu-le pe cele cu distanta minimca mai mare sau egala cu 100 si
		pe cele cu distanta maxima mai mica sau egala cu distanta maxima a unei rute care pleaca din Cluj-Napoca
*/
SELECT R.endingPoint,MIN(R.routeLength),MAX(R.routeLength)
FROM rroute R
GROUP BY R.endingPoint
HAVING MIN(R.routeLength)>=100 AND MAX(R.routeLength)<=(SELECT MAX(R2.routeLength)  
													   FROM rroute R2
													   WHERE R2.startingPoint='Cluj-Napoca'
													  )

/*
	i.WHERE + ANY + AGGREGATION
	Gasiti toate coletele care au greutatea mai mare decat greutatea unui colet care are ca un expeditor cu id maxim
*/
SELECT *
FROM cargo C
WHERE C.cargoWeight> ANY (
							SELECT C2.cargoWeight
							FROM cargo C2
							WHERE C2.seid=MAX(C2.seid)
						 )

/*
	i.WHERE + ALL + AGGREGATION
	Gasiti transporturile care au lungimea mai mica decat media rutelor 
*/
SELECT *
FROM transportRoute TR
WHERE TR.rid < ALL(
						SELECT R.rid
						FROM rroute R
						WHERE R.routeLength=AVG(R.routeLength)
				  )

/*
	i.ALL + IN

*/



--modify the type of a column (truck.capacity: INT -> VARCHAR)
CREATE PROCEDURE uspModifyColumnType
AS
	ALTER TABLE truck
	ALTER COLUMN capacity VARCHAR(10)
GO 
--EXEC uspModifyColumnType

--the oppisite of uspModifyColumnType(truck.capacity: VARCHAR -> INT)
CREATE PROCEDURE uspUndoModifyColumnType
AS
	ALTER TABLE truck
	ALTER COLUMN capacity INT
GO
--EXEC uspUndoModifyColumnType

--add a column (parking: we add a column parkingName)
CREATE PROCEDURE uspAddColumn
AS
	ALTER TABLE parking
	ADD parkingName VARCHAR(50)
GO
--EXEC uspAddColumn

--remove a column(parking: we remove the column parkingName) -> the opposite of the uspAddColumn
CREATE PROCEDURE uspRemoveColumn
AS
	ALTER TABLE parking
	DROP COLUMN parkingName
GO
--EXEC uspRemoveColumn

--add default constraint(receiver.name)
CREATE PROCEDURE uspAddDefaultConstraint
AS
	ALTER TABLE receiver
	ADD CONSTRAINT df_receiverName
	DEFAULT 'Gelu' FOR receiverName
GO
--EXEC uspAddDefaultConstraint

--remove default constraint(receiver.name) -> the opposite of uspAddDefaultConstraint
CREATE PROCEDURE uspRemoveDefaultConstraint
AS
	ALTER TABLE receiver
	DROP CONSTRAINT df_receiverName
GO
--EXEC uspRemoveDefaultConstraint

--add primary key constraint(highway.highwayLength)
CREATE PROCEDURE uspAddPrimaryKeyConstraint
AS
	ALTER TABLE highway
	ADD CONSTRAINT pk_highway PRIMARY KEY (highwayLength)
GO
--EXEC uspAddPrimaryKeyConstraint

--remove primary key constraint(highway.highwayLength) -> the opposite of the uspAddPrimaryKeyConstraint
CREATE PROCEDURE uspRemovePrimaryKeyConstraint
AS
	ALTER TABLE highway
	DROP CONSTRAINT pk_highway
GO
--EXEC uspRemovePrimaryKeyConstraint

--add candidate key constraint(highway.highwayName)
CREATE PROCEDURE uspAddCandidateKeyConstraint
AS
	ALTER TABLE highway
	ADD CONSTRAINT uk_highway UNIQUE (highwayName)
GO
--EXEC uspAddCandidateKeyConstraint

--remove candidate key constraint(highway.highwayName) -> the opposite of addCandidateKeyContraint
CREATE PROCEDURE uspRemoveCandidateKeyConstraint
AS
	ALTER TABLE highway
	DROP CONSTRAINT uk_highway
Go
--EXEC uspRemoveCandidateKeyConstraint

--add foreign key constraint(rroute)
CREATE PROCEDURE uspAddForeignKeyConstraint
AS
	ALTER TABLE rroute
	ADD CONSTRAINT	fk_rroute FOREIGN KEY (hid) 
	REFERENCES highway(hid)
GO
--EXEC uspAddForeignKeyConstraint

--remove foreign key constraint (rroute) -> the opposite of uspAddForeignKeyConstraint
CREATE PROCEDURE uspRemoveForeignKeyConstraint
AS
	ALTER TABLE rroute
	DROP CONSTRAINT fk_rroute
GO
EXEC uspRemoveForeignKeyConstraint

--create a table
CREATE PROCEDURE uspCreateTable
AS
	CREATE TABLE gasStation
		(
			gid INT PRIMARY KEY,
			gasLocation VARCHAR (100),
			gasPrice INT
		)
GO
EXEC uspCreateTable

--remove a table -> the opposite of uspCreateTable
CREATE PROCEDURE uspRemoveTable
AS
	DROP TABLE gasStation
GO
EXEC uspRemoveTable


